import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SliderNewsComponent } from './src/slider-news.component';
import { StreamingActionNewsComponent } from './src/streaming-action-news.component';
import { SwiperModule } from 'angular2-swiper-wrapper';


export * from './src/slider-news.component';
export * from './src/streaming-action-news.component';


@NgModule({
  imports: [
    CommonModule,
    SwiperModule
  ],
  declarations: [

    SliderNewsComponent,
    StreamingActionNewsComponent
  ],
  exports: [

    SliderNewsComponent,
    StreamingActionNewsComponent
  ]
})
export class SliderNewsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SliderNewsModule,
      providers: []
    };
  }
}
